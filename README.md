# 咖啡店网站

#### 项目介绍
本站制作使用了HTML和CSS以及使用的jQuery，同时在多次使用CSS3的动画效果，使得网站具有更多的生动性，同时在客户留言这个内容中，使用了jQuery显示与隐藏及定时器，让客户的留言不断的滚动，方便用户浏览。本网站没有使用插件，纯手写。同时本网站在IE9以上、其他浏览器及大多数PC的分辨率上显示的效果一致。

### 网页地址
https://broken-life.gitee.io/coffeeshop
